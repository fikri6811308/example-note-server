# Example Note Server

This repository contains source code written in express.js that apply notes management application.

## Requirements

- `mongodb`
- `node`
- `yarn`

## Development

In order to run this repository on development, you need to follow the following steps:

1. Prepare dedicated mongodb database for this application.
2. Install dependencies using `yarn install`.
3. Setup `.env` if needed.
4. Run `yarn dev` to run the application on development mode.

## Development with Docker

```
# Start docker containers
docker-compose up -d

# See docker container logs
docker-compose logs -f app
```

## Deployment

Make sure you've built the docker image.

```sh
docker build -t fikrirnurhidayat/example-note-server:latest .
```

To deploy the application, you just need to run docker-compose.yaml inside `deploy` directory.

```sh
# Start docker containers
docker-compose -f deploy/docker-compose.yaml up -d

# See docker container logs
docker-compose -f deploy/docker-compose.yaml logs -f app
```

## Testing

The only testing available on this repository is stress testing. To do a stress test, you need to use [`k6`](https://k6.io/docs/get-started/installation/) application.
When you have `k6` on your machine, run the following command:

```sh
# Make sure development cluster is down
docker-compose down

# Spin up deployment docker cluster
docker-compose -f deploy/docker-compose.yaml up -d

# Get `app` logs
docker-compose -f deploy/docker-compose.yaml logs -f app
```

On another terminal, run the following command:

```
k6 run tests/stress/notes.js
```

# License

The application is available as open source under the terms of the [MIT License](./LICENSE).
