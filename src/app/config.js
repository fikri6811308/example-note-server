module.exports = {
  database: {
    url: process.env.DATABASE_URL,
  },
  security: {
    password: {
      saltRounds: Number(process.env.SECURITY_PASSWORD_SALT_ROUNDS || "10"),
    },
    session: {
      secret: process.env.SECURITY_SESSION_SECRET || "R643z]Gn*=&TbZV%",
      expiresIn: Number(process.env.SECURITY_SESSION_EXPIRES_IN || "3600"),
    },
  },
};
