const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

if (process.env.NODE_ENV !== "production") require("dotenv").config();

const featuresDir = path.join(__dirname, "..", "features");
const infrastucturesDir = path.join(__dirname, "infrastructures");

const app = express();

app.use(express.json());
app.use(cors());
app.use(morgan("dev"));

app.bootstrap = async function bootstrap() {
  const infrastructures = fs.readdirSync(infrastucturesDir);
  const features = fs.readdirSync(featuresDir, { withFileTypes: true });

  await Promise.all(
    infrastructures.map((infrastructurePath) => {
      const infra = require(path.join(infrastucturesDir, infrastructurePath));
      return infra.start();
    })
  );

  features.forEach((feature) => {
    if (!feature.isDirectory()) return;

    const routerPath = path.join(featuresDir, feature.name, "router.js");
    if (!fs.existsSync(routerPath)) return;

    const router = require(routerPath);
    app.use(router);
  });

  app.use(require("./middlewares/handleRouteNotFound"));
  app.use(require("./middlewares/handleException"));
};

module.exports = app;
