import http from "k6/http";

export const options = {
  vus: 10,
  duration: "30s",
};

export default function () {
  http.get("http://localhost:8080/v1/notes", {
    headers: {
      Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZmM2ZTc1NTRjM2ZjY2ExNzA5M2RlMyIsInVzZXJuYW1lIjoiU3RlZmFuaWVfQm9yZXIiLCJleHAiOjYwMzg5NTY4MjY3NjIsImlhdCI6MTY3NzQ4ODAwN30.3vh0P9GiVsHcfmxCdubLhPNXbQhcSkzQZnFRYRBTrBA`,
    },
  });
}
