FROM alpine:latest

RUN apk add --no-cache nodejs npm

RUN npm install -g yarn

WORKDIR /opt/notekeeper

COPY package.json yarn.lock ./

RUN yarn

COPY . .

CMD ["yarn", "start"]
